<?php
	include '../conn.php';
	$id = $_GET['id'];

$queryResult = $connect->
query("
SELECT emergency.id,
emergency.description,
emergency.address,
emergency.emergency_type,
emergency.lat,
emergency.lng,
emergency.status,
creater.id as 'user_id',
creater.full_name as 'user_name',
creater.phone as 'user_phone',
creater.blood_type as 'user_blood_type',
emergency.created_at,
emergency.report,
reporter.id as 'reporter_id',
reporter.full_name as 'reporter_name',
reporter.phone as 'reporter_phone',
reporter.user_type as 'reporter_type',
emergency.report_at
FROM emergency 
LEFT JOIN users as reporter ON emergency.report_by = reporter.id
LEFT JOIN users as creater ON emergency.created_by = creater.id
where emergency.id = $id
") or die($connect->error);

$result=array();

while($fetchData = $queryResult->fetch_assoc()){
	$result[]=$fetchData;
}

echo json_encode($result);

?>
