-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 11, 2019 at 06:27 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emergency218`
--

-- --------------------------------------------------------

--
-- Table structure for table `emergency`
--

CREATE TABLE `emergency` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `emergency_type` varchar(100) NOT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `report` varchar(254) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `report_by` int(11) DEFAULT NULL,
  `report_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emergency`
--

INSERT INTO `emergency` (`id`, `description`, `address`, `emergency_type`, `lat`, `lng`, `status`, `report`, `created_by`, `created_at`, `report_by`, `report_at`) VALUES
(1, 'تفاصيل الحالة', 'مكان الحالة', 'FIRE', 32.8885, 13.187, 'DONE', 'تقرير الحالة', 1, '2019-11-07 09:45:11', 2, '2019-11-05 00:00:00'),
(2, 'ddd', 'ddd', 'THEFT', 32.8885, 13.1, 'PROCESSING', NULL, 1, '2019-11-09 16:17:49', NULL, NULL),
(3, 'dddd', 'dddd', 'FIRE', 37.422, 37.422, 'NEW', NULL, 1, '2019-11-09 21:01:11', NULL, NULL),
(4, 'ffff', 'fff', 'ACCEDENT', 37.422, 37.422, 'NEW', NULL, 1, '2019-11-09 21:01:28', NULL, NULL),
(5, 'fffddd', 'dddd', 'CRIME', 37.422, 37.422, 'NEW', NULL, 1, '2019-11-09 21:01:41', NULL, NULL),
(6, 'dddccc', 'ccccc', 'THEFT', 37.422, 37.422, 'PROCESSING', NULL, 1, '2019-11-09 21:08:07', NULL, NULL),
(7, 'jjj', 'jjjj', 'ACCEDENT', 37.422, 37.422, 'NEW', NULL, 1, '2019-11-09 21:08:54', NULL, NULL),
(8, 'fff', 'ff', 'FIRE', 37.422, 37.422, 'NEW', NULL, 1, '2019-11-09 21:09:34', NULL, NULL),
(9, 'fff', 'ff', 'FIRE', 37.422, 37.422, 'PROCESSING', NULL, 2, '2019-11-09 21:09:42', NULL, NULL),
(10, 'xxxxx', 'xxxxxx', 'CRIME', 37.422, 37.422, 'NEW', NULL, 2, '2019-11-09 21:13:48', NULL, NULL),
(11, 'bbbb', 'bbbb', 'FIRE', 37.422, 37.422, 'NEW', NULL, 1, '2019-11-09 21:17:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(250) NOT NULL,
  `blood_type` varchar(5) DEFAULT NULL,
  `user_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `phone`, `password`, `blood_type`, `user_type`) VALUES
(1, 'osama', '2', '2', 'a+', 'FIRE'),
(2, 'osa', '1', '1', 'B+', 'USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emergency`
--
ALTER TABLE `emergency`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_emergency_user` (`created_by`),
  ADD KEY `fk_report_user` (`report_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emergency`
--
ALTER TABLE `emergency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `emergency`
--
ALTER TABLE `emergency`
  ADD CONSTRAINT `fk_emergency_user` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_report_user` FOREIGN KEY (`report_by`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
